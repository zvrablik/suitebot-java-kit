package suitebot.ai;

import org.junit.Test;
import suitebot.game.Direction;
import suitebot.game.GameState;
import suitebot.game.GameStateFactory;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

/**
 * Created by zvrablik on 4/23/16.
 */
public class BotaTest {

    @Test
    public void shouldAvoidObstacles() throws Exception
    {
        String gameStateAsString =
                " **\n" +
                        " 1*\n" +
                        " **\n";

        GameState gameState = GameStateFactory.createFromString(gameStateAsString);

        assertThat(new Bota().makeMove(1, gameState).step1, is(Direction.LEFT));
    }

    @Test
    public void shouldAvoidObstaclesOnBorder1() throws Exception
    {
        String gameStateAsString =
                "*1**\n"+
                        "****\n"+
                        "****\n"+
                        "* **\n"
                ;

        GameState gameState = GameStateFactory.createFromString(gameStateAsString);

        assertThat(new Bota().makeMove(1, gameState).step1, is(Direction.UP));
    }

    @Test
    public void shouldAvoidObstaclesOnBorder2() throws Exception
    {
        String gameStateAsString =
                "* **\n"+
                        "****\n"+
                        "****\n"+
                        "*1**\n";

        GameState gameState = GameStateFactory.createFromString(gameStateAsString);

        assertThat(new Bota().makeMove(1, gameState).step1, is(Direction.DOWN));
    }

    @Test
    public void shouldAvoidObstaclesOnBorder3() throws Exception
    {
        String gameStateAsString =
                "****\n"+
                        "****\n"+
                        "1** \n"+
                        "****\n";

        GameState gameState = GameStateFactory.createFromString(gameStateAsString);

        assertThat(new Bota().makeMove(1, gameState).step1, is(Direction.LEFT));
    }

    @Test
    public void shouldAvoidOtherBots() throws Exception
    {
        String gameStateAsString =
                "423\n" +
                        " 15\n" +
                        "976\n";

        GameState gameState = GameStateFactory.createFromString(gameStateAsString);

        assertThat(new Bota().makeMove(1, gameState).step1, is(Direction.LEFT));
    }

    @Test
    public void shouldAvoidOtherBots2() throws Exception
    {
        String gameStateAsString =
                "423\n" +
                        "51 \n" +
                        "976\n";

        GameState gameState = GameStateFactory.createFromString(gameStateAsString);

        assertThat(new Bota().makeMove(1, gameState).step1, is(Direction.RIGHT));
    }
}