package suitebot.ai;

import org.junit.Test;
import suitebot.game.GameState;
import suitebot.game.GameStateFactory;
import suitebot.game.Move;
import suitebot.game.Point;

import static org.junit.Assert.*;

/**
 * Created by zvrablik on 4/23/16.
 */
public class DestinationBuilderTest {

    @Test
    public void testBuild() throws Exception {
        String gameStateAsString =
                "* **\n" +
                        "****\n" +
                        "****\n" +
                        "*1**\n";

        GameState gameState = GameStateFactory.createFromString(gameStateAsString);

        DestinationBuilder destBuilder = new DestinationBuilder(gameState, 1);
        Point expectedState = new Point(1,2);
        Point newState = destBuilder.build(Move.UP_);

        assertEquals(expectedState, newState);
    }

    @Test
    public void testBuild1() throws Exception {
        String gameStateAsString =
                        "1****\n" +
                        "*****\n" +
                        "*****\n" +
                        " ****\n";

        GameState gameState = GameStateFactory.createFromString(gameStateAsString);

        DestinationBuilder destBuilder = new DestinationBuilder(gameState, 1);
        Point expectedState = new Point(0,3);
        Point newState = destBuilder.build(Move.UP_);

        assertEquals(expectedState, newState);
    }

    @Test
    public void testBuild2() throws Exception {
        String gameStateAsString =
                        "1*** \n" +
                        "*****\n" +
                        "*****\n" +
                        "*****\n";

        GameState gameState = GameStateFactory.createFromString(gameStateAsString);

        DestinationBuilder destBuilder = new DestinationBuilder(gameState, 1);
        Point expectedState = new Point(4,0);
        Point newState = destBuilder.build(Move.LEFT_);

        assertEquals(expectedState, newState);
    }

    @Test
    public void testBuild3() throws Exception {
        String gameStateAsString =
                "12**5\n" +
                "6****\n" +
                "*****\n" +
                "8***9\n";

        GameState gameState = GameStateFactory.createFromString(gameStateAsString);

        assertEquals(new Point(0,0), gameState.getBotLocation(1));
        assertEquals(new Point(1,0), gameState.getBotLocation(2));
        assertEquals(new Point(4,0), gameState.getBotLocation(5));
        assertEquals(new Point(0,1), gameState.getBotLocation(6));
        assertEquals(new Point(0,3), gameState.getBotLocation(8));
        assertEquals(new Point(4,3), gameState.getBotLocation(9));
    }
}