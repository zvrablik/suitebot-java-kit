package suitebot.json;

import org.junit.Test;
import suitebot.game.GameState;
import suitebot.game.Point;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;

public class JsonUtilTest
{
	private static final String GAME_STATE_JSON =
			"{\n" +
			"  \"yourBotId\": 2,\n" +
			"  \"botIds\": [1, 2, 4],\n" +
			"  \"botEnergyMap\": {\n" +
			"    \"1\": 0,\n" +
			"    \"2\": 15,\n" +
			"    \"4\": 18\n" +
			"  },\n" +
			"  \"gamePlan\": [\n" +
			"    \"*2 *\",\n" +
			"    \"4 +*\",\n" +
			"    \"!   \"\n" +
			"  ]\n" +
			"}";

	@Test
	public void testDeserializeGameState() throws Exception
	{
		GameState gameState = JsonUtil.deserializeGameState(GAME_STATE_JSON);
		assertThat(gameState.getPlanWidth(), is(4));
		assertThat(gameState.getPlanHeight(), is(3));
		assertThat(gameState.getAllBotIds(), containsInAnyOrder(1, 2, 4));
		assertThat(gameState.getLiveBotIds(), containsInAnyOrder(2, 4));
		assertThat(gameState.getBotLocation(1), is(nullValue()));
		assertThat(gameState.getBotLocation(2), equalTo(new Point(1, 0)));
		assertThat(gameState.getBotLocation(4), equalTo(new Point(0, 1)));
		assertThat(gameState.getBotEnergy(1), is(0));
		assertThat(gameState.getBotEnergy(2), is(15));
		assertThat(gameState.getBotEnergy(4), is(18));
		assertThat(gameState.getObstacleLocations(), containsInAnyOrder(
				new Point(0, 0), new Point(3, 0), new Point(3, 1)));
		assertThat(gameState.getBatteryLocations(), containsInAnyOrder(new Point(2, 1)));
		assertThat(gameState.getTreasureLocations(), containsInAnyOrder(new Point(0, 2)));
	}

	@Test
	public void testDeserializeYourBotId() throws Exception
	{
		assertThat(JsonUtil.deserializeYourBotId(GAME_STATE_JSON), is(2));
	}
}
