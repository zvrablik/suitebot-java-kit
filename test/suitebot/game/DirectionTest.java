package suitebot.game;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class DirectionTest
{
	private static final Point FROM = new Point(3, 7);

	@Test
	public void testLeftFrom() throws Exception
	{
		assertThat(Direction.LEFT.from(FROM), equalTo(new Point(2, 7)));
	}

	@Test
	public void testRightFrom() throws Exception
	{
		assertThat(Direction.RIGHT.from(FROM), equalTo(new Point(4, 7)));
	}

	@Test
	public void testUpFrom() throws Exception
	{
		assertThat(Direction.UP.from(FROM), equalTo(new Point(3, 6)));
	}

	@Test
	public void testDownFrom() throws Exception
	{
		assertThat(Direction.DOWN.from(FROM), equalTo(new Point(3, 8)));
	}
}