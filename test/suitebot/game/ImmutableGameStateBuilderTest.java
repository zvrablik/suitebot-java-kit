package suitebot.game;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.Is.is;

public class ImmutableGameStateBuilderTest
{
	private static final int PLAN_WIDTH = 12;
	private static final int PLAN_HEIGHT = 12;
	private static final List<Integer> BOT_IDS = ImmutableList.of(1, 2, 3);
	private static final List<Integer> LIVE_BOT_IDS = ImmutableList.of(1, 2);
	private static final Map<Integer, Point> BOT_LOCATIONS = ImmutableMap.of(
			1, new Point(0, 0),
			2, new Point(11, 11));
	private static final Map<Integer, Integer> BOT_ENERGY = ImmutableMap.of(
			1, 20,
			2, 25,
			3,  0);
	private static final List<Point> OBSTACLES = ImmutableList.of(new Point(1, 1), new Point(2, 2));
	private static final List<Point> BATTERIES = ImmutableList.of(new Point(3, 3), new Point(4, 4));
	private static final List<Point> TREASURES = ImmutableList.of(new Point(4, 5), new Point(6, 7));

	private ImmutableGameState.Builder gameStateBuilder;

	@Before
	public void setUp() throws Exception
	{
		gameStateBuilder = ImmutableGameState.builder()
				.setPlanWidth(PLAN_WIDTH)
				.setPlanHeight(PLAN_HEIGHT)
				.setBotIds(BOT_IDS)
				.setLiveBotIds(LIVE_BOT_IDS)
				.setBotLocationMap(BOT_LOCATIONS)
				.setBotEnergyMap(BOT_ENERGY)
				.setObstacles(OBSTACLES)
				.setBatteries(BATTERIES)
				.setTreasures(TREASURES);
	}

	@Test
	public void testBuild() throws Exception
	{
		GameState gameState = gameStateBuilder.build();

		assertThat(gameState.getPlanWidth(), is(PLAN_WIDTH));
		assertThat(gameState.getPlanHeight(), is(PLAN_HEIGHT));
		assertThat(gameState.getAllBotIds(), containsInAnyOrder(BOT_IDS.toArray()));
		assertThat(gameState.getLiveBotIds(), containsInAnyOrder(LIVE_BOT_IDS.toArray()));
		assertThat(gameState.getObstacleLocations(), containsInAnyOrder(OBSTACLES.toArray()));
		assertThat(gameState.getBatteryLocations(), containsInAnyOrder(BATTERIES.toArray()));
		assertThat(gameState.getTreasureLocations(), containsInAnyOrder(TREASURES.toArray()));
		assertThat(gameState.getBotLocation(1), equalTo(new Point(0, 0)));
		assertThat(gameState.getBotEnergy(2), is(25));
	}

	@Test
	public void testBuild_fromGameState() throws Exception
	{
		GameState sourceGameState = gameStateBuilder.build();
		GameState gameState = ImmutableGameState.builder(sourceGameState).build();

		assertThat(gameState.getPlanWidth(), is(PLAN_WIDTH));
		assertThat(gameState.getPlanHeight(), is(PLAN_HEIGHT));
		assertThat(gameState.getAllBotIds(), containsInAnyOrder(BOT_IDS.toArray()));
		assertThat(gameState.getLiveBotIds(), containsInAnyOrder(LIVE_BOT_IDS.toArray()));
		assertThat(gameState.getObstacleLocations(), containsInAnyOrder(OBSTACLES.toArray()));
		assertThat(gameState.getBatteryLocations(), containsInAnyOrder(BATTERIES.toArray()));
		assertThat(gameState.getTreasureLocations(), containsInAnyOrder(TREASURES.toArray()));
		assertThat(gameState.getBotLocation(1), equalTo(new Point(0, 0)));
		assertThat(gameState.getBotEnergy(2), is(25));
	}

	@Test(expected = ImmutableGameState.UnableToBuildException.class)
	public void missingBotIds_shouldThrowException() throws Exception
	{
		gameStateBuilder.setBotIds(null).build();
	}

	@Test(expected = ImmutableGameState.UnableToBuildException.class)
	public void missingBotLocations_shouldThrowException() throws Exception
	{
		gameStateBuilder.setBotLocationMap(null).build();
	}

	@Test(expected = ImmutableGameState.UnableToBuildException.class)
	public void locationNotSet_forAnyBot_shouldThrowException() throws Exception
	{
		gameStateBuilder.setBotLocationMap(ImmutableMap.of(1, new Point(0, 0))).build();
	}

	@Test(expected = ImmutableGameState.UnableToBuildException.class)
	public void missingEnergyMap_shouldThrowException() throws Exception
	{
		gameStateBuilder.setBotEnergyMap(null).build();
	}

	@Test(expected = ImmutableGameState.UnableToBuildException.class)
	public void energyNotSet_forAnyLiveBot_shouldThrowException() throws Exception
	{
		gameStateBuilder.setBotEnergyMap(ImmutableMap.of(2, 25)).build();
	}

	@Test(expected = ImmutableGameState.UnableToBuildException.class)
	public void nonPositivePlanWidth_shouldThrowException() throws Exception
	{
		gameStateBuilder.setPlanWidth(-1).build();
	}

	@Test(expected = ImmutableGameState.UnableToBuildException.class)
	public void nonPositivePlanHeight_shouldThrowException() throws Exception
	{
		gameStateBuilder.setPlanHeight(0).build();
	}

	@Test(expected = ImmutableGameState.UnableToBuildException.class)
	public void liveBots_notSubsetOf_allBots_shouldThrowException() throws Exception
	{
		gameStateBuilder.setLiveBotIds(ImmutableSet.of(1, 2, 3, 4)).build();
	}

	@Test(expected = ImmutableGameState.UnableToBuildException.class)
	public void nonUniqueBotIds_shouldThrowException() throws Exception
	{
		gameStateBuilder.setBotIds(ImmutableList.of(2, 2)).build();
	}

	@Test(expected = ImmutableGameState.UnableToBuildException.class)
	public void nonUniqueBotLocations_shouldThrowException() throws Exception
	{
		Map<Integer, Point> duplicateBotLocations = ImmutableMap.of(
				1, new Point(0, 0),
				2, new Point(0, 0));
		gameStateBuilder.setBotLocationMap(duplicateBotLocations).build();
	}

	@Test(expected = ImmutableGameState.UnableToBuildException.class)
	public void locationOccupiedByMultipleObjects_shouldThrowException_1() throws Exception
	{
		// collides with the location of the bot 1
		gameStateBuilder.setObstacles(ImmutableSet.of(new Point(0, 0))).build();
	}

	@Test(expected = ImmutableGameState.UnableToBuildException.class)
	public void locationOccupiedByMultipleObjects_shouldThrowException_2() throws Exception
	{
		// collides with the location of a battery
		gameStateBuilder.setTreasures(ImmutableSet.of(new Point(3, 3))).build();
	}
}
