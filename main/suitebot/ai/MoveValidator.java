package suitebot.ai;

import suitebot.game.GameState;
import suitebot.game.Move;
import suitebot.game.Point;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by zvrablik on 4/23/16.
 */
public class MoveValidator {
    private final Integer botId;
    private final GameState gameState;

    public MoveValidator(Integer botId, GameState state){
        this.botId = botId;
        this.gameState = state;
    }

    public boolean validate(Point point){
        boolean isValid = !gameState.getObstacleLocations().contains(point);

        if (isValid){
            isValid = !moveToOtherBotHead(point);
        }

        return isValid;
    }

    private boolean moveToOtherBotHead(Point destination) {
        Set<Integer> liveBotIds = gameState.getLiveBotIds();

        Set<Point> headsPositions = new HashSet<>(liveBotIds.size());

        for (Integer oneBotId : liveBotIds){
            headsPositions.add(gameState.getBotLocation(oneBotId));
        }

        return headsPositions.contains(destination);
    }
}
