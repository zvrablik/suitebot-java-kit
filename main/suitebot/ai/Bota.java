package suitebot.ai;

import com.google.common.collect.ImmutableList;
import suitebot.game.GameState;
import suitebot.game.Move;
import suitebot.game.Point;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Created by zvrablik on 4/23/16.
 */
public class Bota implements BotAi {

    private static final List<Move> SINGLE_MOVES = ImmutableList.of(
            Move.LEFT_, Move.UP_, Move.RIGHT_, Move.DOWN_);

    @Override
    public Move makeMove(int botId, GameState gameState) {

        if (isDead(botId, gameState)) {
            return null;
        }

        MoveValidator validator = new MoveValidator(botId, gameState);
        DestinationBuilder destBuilder = new DestinationBuilder(gameState, botId);

        Iterator<Move> itMoves = SINGLE_MOVES.iterator();
        while (itMoves.hasNext()) {
            Move move = itMoves.next();

            if (validator.validate(destBuilder.build(move))){
                return move;
            }
        }

        return null;//game over :)
    }

    private boolean isDead(Integer botId, GameState gameState)
    {
        return !gameState.getLiveBotIds().contains(botId);
    }

    @Override
    public String getName() {
        return "zvrablik";
    }
}
