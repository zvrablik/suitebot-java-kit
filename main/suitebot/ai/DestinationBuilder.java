package suitebot.ai;

import suitebot.game.GameState;
import suitebot.game.Move;
import suitebot.game.Point;

/**
 * Created by zvrablik on 4/23/16.
 */
public class DestinationBuilder {

    private final int botId;
    private final GameState gameState;

    public DestinationBuilder(GameState gameState, int botId) {
        this.gameState = gameState;
        this.botId = botId;
    }

    /**
     * Get destination point.
     *
     * @param move
     * @return
     */
    public Point build(Move move)
    {
        int planHeight = gameState.getPlanHeight();
        int planWidth = gameState.getPlanWidth();
        Point botLocation = gameState.getBotLocation(botId);
        Point step1Destination = move.step1.from(botLocation);

        if (move.step2 == null)
            return fixPoint(planHeight, planWidth, step1Destination);

        Point from = move.step2.from(step1Destination);

        from = fixPoint(planHeight, planWidth, from);

        return from;
    }

    private Point fixPoint(int planHeight, int planWidth, Point from) {
        if (from.x < 0){
            from = new Point(planWidth - 1, from.y);
        }

        if (from.y < 0){
            from = new Point(from.x, planHeight - 1);
        }
        return from;
    }
}
